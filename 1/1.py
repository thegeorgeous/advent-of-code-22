#!/usr/bin/env python3

total_calories = [0]

i = 0

input_file = open("input.txt", "r")

for line in input_file.readlines():
   if not line.strip():
       i += 1
       total_calories.append(0)
   else:
       total_calories[i] += int(line)

total_calories.sort()
print(total_calories[-1])
print(sum(total_calories[-3:]))
