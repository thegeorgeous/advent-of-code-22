#!/usr/bin/env python3

SMALLCASE_OFFSET = 96
UPPERCASE_OFFSET = 38

input_file = open("input.txt", "r")

common = []
for line in input_file.readlines():
    length = len(line) - 1
    skip_line = False
    priority = 0
    for x in range(length//2):
        for y in range(length//2, length):
            if line[x] == line[y]:
                if line[x].isupper():
                    priority = ord(line[x]) - UPPERCASE_OFFSET
                else:
                    priority = ord(line[x]) - SMALLCASE_OFFSET
                common.append(priority)
                skip_line = True
                break
        if skip_line:
            break

print(sum(common))
