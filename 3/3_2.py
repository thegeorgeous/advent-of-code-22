#!/usr/bin/env python3

SMALLCASE_OFFSET = 96
UPPERCASE_OFFSET = 38

input_file = open("input.txt", "r")

badges = []

def prioritize(x):
    priority = 0
    if x.isupper():
        priority = ord(x) - UPPERCASE_OFFSET
    else:
        priority = ord(x) - SMALLCASE_OFFSET
    return priority

group = []

for line in input_file.readlines():
    group.append(line.strip())
    if len(group) == 3:

        first_common = list(set(group[0]).intersection(group[1]))
        badge = list(set(first_common).intersection(group[2]))
        if len(badge) > 0:
            badges.append(badge[0])
        group = []

print(sum(map(prioritize, badges)))
