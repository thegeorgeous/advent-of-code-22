#!/usr/bin/env python3

ROCK = 1
PAPER = 2
SCISSORS = 3

player1 = {
    'A': ROCK,
    'B': PAPER,
    'C': SCISSORS
}

player2 = {
    'X': ROCK,
    'Y': PAPER,
    'Z': SCISSORS
}

rules = {
    ROCK: PAPER,
    PAPER: SCISSORS,
    SCISSORS: ROCK
}

input_file = open("input.txt", "r")

score = 0
winning_score = 6
draw_score = 3
loss_score = 0

for line in input_file.readlines():
    inputs = line.strip().split(" ")
    player1_input = player1[inputs[0]]
    player2_input = player2[inputs[1]]

    if player1_input == player2_input:
        score += player2_input + draw_score
    elif rules[player1_input] == player2_input:
       score += player2_input + winning_score
    else:
        score += player2_input + loss_score

print(score)
