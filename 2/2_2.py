#!/usr/bin/env python3

#!/usr/bin/env python3

ROCK = 1
PAPER = 2
SCISSORS = 3

player1 = {
    'A': ROCK,
    'B': PAPER,
    'C': SCISSORS
}

outcomes = {
    'X': 0,
    'Y': 3,
    'Z': 6
}

rules = {
    ROCK: PAPER,
    PAPER: SCISSORS,
    SCISSORS: ROCK
}

inverse_rules = {
    ROCK: SCISSORS,
    PAPER: ROCK,
    SCISSORS: PAPER
}

input_file = open("input.txt", "r")

score = 0
winning_score = 6
draw_score = 3
loss_score = 0

for line in input_file.readlines():
    inputs = line.strip().split(" ")
    player1_input = player1[inputs[0]]

    if inputs[1] == 'X':
        score += inverse_rules[player1_input] + outcomes[inputs[1]]
    elif inputs[1] == 'Y':
        score += player1_input + outcomes[inputs[1]]
    else:
        score += rules[player1_input] + outcomes[inputs[1]]

print(score)
